# Minesweeper Style Game #
*Only Html and CSS were used to create this game.*
Inspired by student project. I attempt to make my own version of a games they have submitted using only HTML and CSS.

## Possible improvments ##
 - Consider adding checkbos to "flag" bombs, so when checked instead of uncovering it places a flag (and possibly prevents the click?)
 
---

## Extra files used
List of files besides out index.html and style.css
1. Google Fonts (Poppins, Roboto) to make it look a little nicer
2. Font DS-Digital (https://www.dafont.com/ds-digital.font) for the score font to give a Minesweeper feel
3. PHP script and Excel file used to help create the HTML for the game board instead of manually writing out every singel cell. (includes the EXCEL files used to plan layout of boards)
---

## JavaScript
If I had include JavScript, what would be different?
- I could allow right-clicks to tag bombs (like the original minesweeper)
- I could automatically select all the empty/zero cells if one was selected (like the original minesweeper)
- I could (maybe) have generated random boards
- I could count the bomb tag instead of selected cells (like the original minesweeper)
- I could have an easier win condition verification process
